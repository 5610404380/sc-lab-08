package control;

import java.util.ArrayList;

import interfaces.Measurable;
import interfaces.Taxable;
import model.BankAccount;
import model.Company;
import model.Country;
import model.Data;
import model.Person;
import model.Product;
import model.TaxCalculator;

public class TestCase {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TestCase testcase = new TestCase() ;
		testcase.testPer() ;
		System.out.println("\n") ;
		testcase.testBCPmin() ;
		System.out.println("\n") ;
		testcase.testTax() ;
	}
	public void testPer(){
		Measurable[] persons = new Measurable[3];
		persons[0] = new Person("Make",173,3950000);
		persons[1] = new Person("Mark",175,50);
		persons[2] = new Person("Noey",154,4753600);

		System.out.println("Average tall : " + Data.average(persons));
	}
	public void testBCPmin(){
		Measurable[] tomin = new Measurable[3];
		Measurable[] persons = new Measurable[6];
		persons[0] = new Person("Make",173,3950000);
		persons[1] = new Person("Noey",154,4753600);
		persons[2] = new BankAccount("Thaipanit",3950000);
		persons[3] = new BankAccount("Kasikorn",50);
		persons[4] = new Country("Thai",4753600);
		persons[5] = new Country("England",50);
		
		tomin[0] =  Data.min(persons[0],persons[1]);
		tomin[1] =  Data.min(persons[2],persons[3]);
		tomin[2] =  Data.min(persons[4],persons[5]);
		System.out.println("Min tall : "+tomin[0].getMeasure());
		System.out.println("Min Bank : "+tomin[1].getMeasure());
		System.out.println("Min Country : "+tomin[2].getMeasure());
		
	}
	public void testTax(){
		ArrayList<Taxable> per = new ArrayList<Taxable>();
		ArrayList<Taxable> com = new ArrayList<Taxable>();
		ArrayList<Taxable> pro = new ArrayList<Taxable>();
		ArrayList<Taxable> tax = new ArrayList<Taxable>();
		
		per.add(new Person("a",150,300000));
				
		com.add(new Company("Company",1000000,800000));
		
		pro.add(new Product("item",100));
		
		tax.add(new Person("a",150,300000));
		tax.add(new Company("Company1",1000000,800000));
		tax.add(new Product("item1",100));
		
		System.out.println("Person sum tax : "+TaxCalculator.sum(per));
		System.out.println("Company sum tax : "+TaxCalculator.sum(com));
		System.out.println("Product sum tax : "+TaxCalculator.sum(pro));
		System.out.println("All sum tax : "+TaxCalculator.sum(tax));
	}
}
