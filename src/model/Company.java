package model;
import interfaces.Taxable;

public class Company implements  Taxable{
	String name;
	double income;
	double outcome;
	
	public Company(String name, double income, double outcome){
		this.name = name;
		this.income = income;
		this.outcome = outcome;
	}
	@Override
	public double getTax() {
		double profit = income - outcome;
		double tax = profit*30/100;
		return tax;
	}
}
