package model;

import interfaces.Measurable;

public class Data {
	public static double average(Measurable[] i){
		double sum = 0;
		for (Measurable m: i){
			sum += m.getMeasure();
		}
		if(i.length>0){
			sum = sum/i.length;
		}
		return sum;
	}

	public static Measurable min(Measurable m1, Measurable m2){
		if (m1.getMeasure() < m2.getMeasure()){
			return m1;
		}
		else{
			return m2;
		}
	}
}
